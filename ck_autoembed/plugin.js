/**
 * @file
 * The functionality of our CKEditor plugin "ck_autoembed".
 */

(function($) {
  CKEDITOR.plugins.add('ck_autoembed', {
    init: function(editor) {
      CKEDITOR.on('instanceReady', function(evt) {
        if ($('.ck-autoembed-loader').length == 0) {
          var selector = $('.form-textarea-wrapper .cke_inner');
          selector.css('position','relative');
          selector.append('<div class="ck-autoembed-loader" style="display:none; position:absolute; left: 45%; top: 50%;"><div style="margin: 0 auto;width: 50px;"><img width="50" height="50" src="/' + Drupal.settings.ckautoembed_path + '/ck_autoembed/images/ajaxSpinner.gif"></div><div style="text-align: center;">Please Wait...</div></div>');
        };
      });
      editor.on('paste', function(e) {
        var pasted = e.data.dataValue;
        var pasted_obj = jQuery('' + pasted);
        var raw = pasted_obj.length == 0 ? pasted : pasted_obj.text();
        if (raw.split("https://").length > 1 || raw.split("http://").length > 1) {
          $('.ck-autoembed-loader').fadeIn().delay(1500).fadeOut();
          $.ajax({
            type: 'GET',
            url: '/ajax/embed?url=' + raw,
            async: false,
            success: function(text) {
              if (text != false) {
                e.data.dataValue = text;
              };
            }
          });
        }
      });
    }
  });
})(jQuery);
