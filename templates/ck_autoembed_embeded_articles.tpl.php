<?php
/**
 * @file
 * This template handles the layout for any embedded article.
 *
 * We're using the parserapp.com service by default to handle those sites
 * and return back some variables that we can handle from our side.
 *
 * Variables available:
 * - $url: The URL title the we are going to embed.
 * - $title: The title of the article.
 * - $description: A small description about the article.
 * - $images: The article images.
 * - $image: The article main image.
 * - $imageWidth: The article main image width.
 * - $imageHeight: The article main image height.
 */
?>
<p><div class="bay-embedded-article" style="border: 1px solid #ccc; width: 825px; padding: 10px;">
  <div class="article-image" style="float: left; margin-right: 20px;">
    <img width="480" height="270" src="<?php print $image; ?>">
  </div>
  <div class="article-body">
    <h2 class="article-title"><?php print $title; ?></h2>
    <p class="article-desc"><?php print $description; ?></p>
  </div>
  <div class="article-readmore" style="clear: both; text-align: right;">
    <a class="article-link" target="_blank" href="<?php print $url; ?>">Read more</a>
  </div>
</div></p>
