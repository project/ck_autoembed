INTRODUCTION
------------
This module will create a plugin for CKEditor that will provide a
direct auto-embed functionality, Just paste the link in your editor
and it will be automatically embedded, It's very much similar to
the WordPress behaviour.

By default we're using the ParserApp embedding service, which pretty
much can embed anything, even normal articles, Please check the
Parser project for more information
(https://www.drupal.org/sandbox/jdaglees/1774752).

REQUIREMENTS
------------
This module requires CKEditor.
https://www.drupal.org/project/ckeditor

INSTALLATION
------------
Just download and enable the module and, and don't forget to
enable the plugin from the CKEditor configuration page
(admin/config/content/ckeditor/edit/Full) under the "EDITOR
APPEARANCE" section under "Plugins" just check the "Plugin to provide
an AutoEmbed functionality for ckeditor." option and save.

You can change the embedding service from CKEditor AutoEmbed Service
config page (admin/config/content/ck_autoembed).

Please note that this plugin needs a Full HTML text formater or you
can enable some specific HTML tags in your text formater:
  - <iframe>
  - <object>
  - <script>
  - <embed>
