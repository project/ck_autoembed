<?php
/**
 * @file
 * Callbacks and include functions for ck_autoembed. Most of the logic defined.
 */

/**
 * Menu callback; for ajax/embed.
 *
 * Processing functionality for the embeding request.
 */
function ck_autoembed_ajax_embed_process() {
  $parameters = drupal_get_query_parameters();
  // Sending a request to parserapp.com. which is the service that we use
  // to get the html embed code from any site.
  $response = drupal_http_request(variable_get('ck_autoembed_embed_service', 'http://parserapp.com/parse.php') . '?url=' . $parameters['url']);
  // Decoding the response.
  $decoded_data = json_decode($response->data);
  // Making sure that we have an ifram html.
  if (isset($decoded_data->code)) {
    drupal_json_output($decoded_data->code);
  }
  else {
    // Rendering the article using our own theme function.
    $rendered_article = theme('ck_autoembed_embeded_articles', array(
      'url'         => $decoded_data->url,
      'title'       => $decoded_data->title,
      'description' => $decoded_data->description,
      'images'      => $decoded_data->images,
      'image'       => $decoded_data->image,
      'imageWidth'  => $decoded_data->imageWidth,
      'imageHeight' => $decoded_data->imageHeight,
    ));
    drupal_json_output($rendered_article);
  }
}

/**
 * Menu callback; for admin/config/content/ck_autoembed.
 *
 * Administration settings for CKEditor AutoEmbed.
 */
function ck_autoembed_admin_settings() {
  $form = array();

  $form['ck_autoembed_embed_service'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Embedding Service'),
    '#default_value' => variable_get('ck_autoembed_embed_service', 'http://parserapp.com/parse.php'),
    '#size'          => 60,
    '#description'   => t("The full URL for the embedding service that you want to use, this URL will be automatically followed by the url parameter '?url='.<br />Please note that the default service that we're using here is from <a href='https://www.drupal.org/sandbox/jdaglees/1774752'>ParserApp</a>."),
    '#required'      => TRUE,
  );

  return system_settings_form($form);
}
